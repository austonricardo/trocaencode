import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;


public class Encoding {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File pasta = new File("C:\\xampp\\htdocs\\publicacoes_normas");
		String exts = ".php;.html;.htm";
		//String encodingFromName = "windows-1252";
		String encodingToName = "ISO-8859-1";
		String encodingFromName = "utf-8";
		//reescreve("C:\\xampp\\htdocs\\protocolo\\index.php","C:\\xampp\\htdocs\\protocolo\\index2.php", encodingFromName, encodingToName);
		
		checaPasta(pasta, pasta.getAbsolutePath(), exts.split(";"),encodingFromName, encodingToName);
	}
	
	private static void reescreve(String arq1, String arq2, String encodingFromName, String encodingToName)
	{
		/*
		*/
		try {
			Reader reader = new InputStreamReader(new FileInputStream(arq1) , Charset.forName(encodingFromName));
			Writer writer = new OutputStreamWriter(new FileOutputStream(arq2), Charset.forName(encodingToName));

			//Copiar stream
			int count;
			char[] buffer = new char[8192];
			while ((count = reader.read(buffer)) > 0)
				writer.write(buffer, 0, count);
			writer.flush();
			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	
	static void checaPasta(File item, String inicio, String[] exts, String encodingFromName, String encodingToName) {
	    File[] subItem = item.listFiles();
	    String novoCaminho = item.getAbsolutePath().replace(inicio,inicio+encodingToName);
	    if (subItem != null) {
	    	//Item � Pasta
	    	if(!item.getAbsolutePath().endsWith(".svn"))
	    	{
		    	//Cria pasta c�pia
		    	File pastaCopia = new File(novoCaminho);
		    	pastaCopia.mkdirs();
		        for (File child : subItem) {
		            checaPasta(child, inicio, exts, encodingFromName, encodingToName);
		        }
	    	}
	    }else{
	    	//Item � arquivo
	    	for(String ext:exts)
	    	{
	    		if(item.getAbsolutePath().endsWith(ext))
	    		{
		    		reescreve(item.getAbsolutePath(),novoCaminho, encodingFromName, encodingToName);
		    		break;
	    		}
	    	}
	    }
	}
	

}
